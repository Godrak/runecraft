# Runecraft

Pattern-recognition spell system for usage in games. Implemented along with a simple game
to showcase the functionality. 

All information is available in the text of the thesis (/Text/thesis.pdf).

## Screenshots
    
### Healing totem

![waterDrop](/Text/Tex/ext/scr/waterDrop.png)
![waterDropExecuted](/Text/Tex/ext/scr/waterDrope.png)


### Shielding totem

![Circle](/Text/Tex/ext/scr/circle.png)
![CircleExecuted](/Text/Tex/ext/scr/circlee.png)

### Combined totem - shooting and fire

![TriangleOfTriangles](/Text/Tex/ext/scr/pattriangle.png)
![TriangleOfTrianglesExecuted](/Text/Tex/ext/scr/pattrianglee.png)



